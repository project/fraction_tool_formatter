<?php
/**
 * @file
 *  Fraction Tool Size API functions for formatters.
 *
 *  © 2015 RedBottle Design, LLC and Five Star Tool Company.
 *  All rights reserved. This code is licensed under the GNU GPL v2 or later.
 *
 * @author Guy Paddock (guy.paddock@redbottledesign.com)
 */

/***************************************************************
 * Hook implementations
 ***************************************************************/
/**
 * Implements of hook_field_info().
 */
function fraction_tool_formatter_field_formatter_info() {
  return array(
    FRTO_FORMATTER_TYPE_TOOL_SIZE => array(
      'label'       => t('Tool size'),
      'field types' => array(FRTO_FIELD_TYPE_FRACTION),
      'settings'    => _fraction_tool_formatter_settings_defaults(),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function fraction_tool_formatter_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display  = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = '';

  if ($display['type'] == FRTO_FORMATTER_TYPE_TOOL_SIZE) {
    $settings = $settings + _fraction_tool_formatter_settings_defaults();

    $format_pattern         = $settings[FRTO_SETTING_FORMAT_PATTERN];

    $whole_number_separator = $settings[FRMI_SETTING_WHOLE_NUMBER_SEPARATOR];
    $fraction_separator     = $settings[FRMI_SETTING_FRACTION_SEPARATOR];

    $decimal_precision      = $settings[FRTO_SETTING_DECIMAL_PRECISION];

    $wire_prefix            = $settings[FRTO_SETTING_WIRE_PREFIX];
    $wire_suffix            = $settings[FRTO_SETTING_WIRE_SUFFIX];

    $summary =
      t('Format pattern: @format_pattern<br />Fraction format: 1@whole_separator2@fraction_separator3<br />Decimal precision: @decimal_precision',
        array(
          '@format_pattern'     => $format_pattern,
          '@whole_separator'    => $whole_number_separator,
          '@fraction_separator' => $fraction_separator,
          '@decimal_precision'  => $decimal_precision,
        ));

    if (!empty($wire_prefix)) {
      $summary .=
        t('<br />Wire prefix: @wire_prefix', array('@wire_prefix' => $wire_prefix));
    }

    if (!empty($wire_suffix)) {
      $summary .=
        t('<br />Wire suffix: @wire_suffix', array('@wire_suffix' => $wire_suffix));
    }
  }

  return $summary;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function fraction_tool_formatter_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display  = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element = array();

  if ($display['type'] == FRTO_FORMATTER_TYPE_TOOL_SIZE) {
    $element = _fraction_tool_formatter_separator_settings_form($settings);
  }

  return $element;
}

/**
 * Implements hook_field_formatter_view().
 */
function fraction_tool_formatter_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $settings = $display['settings'];
  $element = array();

  // Iterate through the items.
  foreach ($items as $delta => $item) {
    // Merge the item with default values, in case it's empty.
    if (($display['type'] == FRTO_FORMATTER_TYPE_TOOL_SIZE) &&
        !fraction_field_is_empty($item, $field)) {
      $value = _fraction_tool_formatter_format_fraction($item['numerator'], $item['denominator'], $settings);

      $element[$delta] = array(
        '#type'   => 'markup',
        '#markup' => $value,
      );
    }
  }

  return $element;
}

/***************************************************************
 * Internal functions
 ***************************************************************/
/**
 * Internal convenience function for building the settings form used by
 * the widget, formatter, and feeds target forms to specify the mixed number
 * separators.
 *
 * @param array $settings
 *   The widget, formatter, or feed target settings array.
 *
 * @return array
 *   The form elements for configuring the separators.
 */
function _fraction_tool_formatter_separator_settings_form(array $settings) {
  $settings = $settings + _fraction_tool_formatter_settings_defaults();

  // Start with the same form that mixed fractions use.
  $form = _fraction_mixed_separator_settings_form($settings);

  // NOTE: Format pattern precedes the settings defined by the mixed-fraction module.
  $form[FRTO_SETTING_FORMAT_PATTERN] = array(
    '#type'          => 'textfield',
    '#title'         => t('Format pattern'),
    '#description'   =>
      t('Specify the pattern that values should follow for display, using the following tokens:') .
      '<dl>' .
        '<dt>[fraction]</dt>' .
        '<dd>' . t('The whole-number fraction value (for example, "1-2/3"), respecting the whole number and fraction separator settings specified below.') . '</dd>' .

        '<dt>[decimal]</dt>' .
        '<dd>' . t('The decimal value of the fraction (for example, "1.66"), respecting the precision setting specified below.') . '</dd>' .

        '<dt>[wire-size]</dt>' .
        '<dd>' . t('The wire or letter size value of the fraction, if one exists (for example, "A"), respecting the prefix and suffix settings specified below.') . '</dd>' .
      '</dl>',

    '#default_value' => $settings[FRTO_SETTING_FORMAT_PATTERN],
    '#weight'        => -1,
  );

  $form[FRTO_SETTING_DECIMAL_PRECISION] = array(
    '#type'             => 'textfield',
    '#title'            => t('Decimal precision'),
    '#description'      => t('Specify the number of digits that should appear after the decimal point.'),
    '#default_value'    => $settings[FRTO_SETTING_DECIMAL_PRECISION],
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form[FRTO_SETTING_WIRE_PREFIX] = array(
    '#type'             => 'textfield',
    '#title'            => t('Wire size prefix'),
    '#description'      => t('Specify any text that should precede the wire size (when it appears).'),
    '#default_value'    => $settings[FRTO_SETTING_WIRE_PREFIX],
  );

  $form[FRTO_SETTING_WIRE_SUFFIX] = array(
    '#type'             => 'textfield',
    '#title'            => t('Wire size suffix'),
    '#description'      => t('Specify any text that should follow the wire size (when it appears).'),
    '#default_value'    => $settings[FRTO_SETTING_WIRE_SUFFIX],
  );

  return $form;
}

/**
 * Internal convenience function for converting a numerator and denominator
 * into a formatted string value, honoring the provided formatter settings.
 *
 * @param string $numerator
 *   The numerator of the fraction.
 *
 * @param string $denominator
 *   The denominator of the fraction.
 *
 * @param array $settings
 *  The formatter settings.
 *
 * @return string
 *   The resulting formatted number.
 */
function _fraction_tool_formatter_format_fraction($numerator, $denominator, array $settings) {
  $settings = $settings + _fraction_tool_formatter_settings_defaults();

  $format_pattern         = $settings[FRTO_SETTING_FORMAT_PATTERN];
  $decimal_precision      = $settings[FRTO_SETTING_DECIMAL_PRECISION];
  $wire_prefix            = $settings[FRTO_SETTING_WIRE_PREFIX];
  $wire_suffix            = $settings[FRTO_SETTING_WIRE_SUFFIX];

  $whole_number_separator = $settings[FRMI_SETTING_WHOLE_NUMBER_SEPARATOR];
  $fraction_separator     = $settings[FRMI_SETTING_FRACTION_SEPARATOR];

  $fraction = mixed_fraction($numerator, $denominator);

  $fraction_value = $fraction->toMixedNumber($whole_number_separator, $fraction_separator);
  $decimal_value  = $fraction->toDecimal($decimal_precision);
  $wire_value     = fraction_tool_formatter_get_wire_size($decimal_value);

  if (!empty($wire_value)) {
    $wire_value = $wire_prefix . $wire_value . $wire_suffix;
  }

  $value =
    strtr(
      $format_pattern,
      array(
        '[fraction]'  => $fraction_value,
        '[decimal]'   => $decimal_value,
        '[wire-size]' => $wire_value,
      ));

  return $value;
}

/**
 * Gets the equivalent wire size for the specified decimal value, if one exists.
 *
 * @param float $decimal_value
 *  The decimal value for which a wire size is desired.
 *
 * @return NULL|string
 *  Either the wire size (80 through 1, or A-Z); or, NULL if there is no wire
 *  size that corresponds to the provided decimal value.
 */
function fraction_tool_formatter_get_wire_size($decimal_value) {
  static $wire_sizes;

  // Cache the table, since it's expensive to create.
  if (empty($wire_sizes)) {
    $wire_sizes = fraction_tool_formatter_get_wire_size_table();
  }

  $decimal_value = sprintf("%01.4f", $decimal_value);

  if (isset($wire_sizes[$decimal_value])) {
    $result = $wire_sizes[$decimal_value];
  }
  else {
    $result = NULL;
  }

  return $result;
}

/**
 * Internal convenience function for getting the default mixed number separator
 * settings.
 *
 * @return array
 *   The default settings for the mixed number widget, formatter, or feed
 *   target.
 */
function _fraction_tool_formatter_settings_defaults() {
  $defaults = _fraction_mixed_separator_settings_defaults();

  $defaults[FRTO_SETTING_FORMAT_PATTERN]    = '[fraction] ([decimal][wire-size])';
  $defaults[FRTO_SETTING_DECIMAL_PRECISION] = 4;
  $defaults[FRTO_SETTING_WIRE_PREFIX]       = ' - ';
  $defaults[FRTO_SETTING_WIRE_SUFFIX]       = '';

  return $defaults;
}