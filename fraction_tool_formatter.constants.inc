<?php
/**
 * @file
 *  Constants for the "Fraction tool size formatter" module.
 *
 *  © 2015 RedBottle Design, LLC and Five Star Tool Company.
 *  All rights reserved. This code is licensed under the GNU GPL v2 or later.
 */

/**
 * The internal machine name of this module.
 */
define('FRTO_MODULE_NAME', 'fraction_tool_formatter');

/**
 * The machine name of fraction fields.
 *
 * @var string
 */
define('FRTO_FIELD_TYPE_FRACTION', 'fraction');

/**
 * The machine name of the tool field formatter.
 *
 * @var string
 */
define('FRTO_FORMATTER_TYPE_TOOL_SIZE', 'fraction_tool_size');

/**
 * The name of the setting that specifies the format that the user would like
 * to use when displaying tool sizes.
 *
 * @var string
 */
define('FRTO_SETTING_FORMAT_PATTERN', 'format_pattern');

/**
 * The name of the setting that specifies number of digits of precision to use
 * when formatting a fraction as a decimal value.
 *
 * @var string
 */
define('FRTO_SETTING_DECIMAL_PRECISION', 'decimal_precision');

/**
 * The name of the setting that specifies what text should precede the wire
 * size value, when it exists.
 *
 * @var string
 */
define('FRTO_SETTING_WIRE_PREFIX', 'wire_prefix');

/**
 * The name of the setting that specifies what text should follow the wire
 * size value, when it exists.
 *
 * @var string
 */
define('FRTO_SETTING_WIRE_SUFFIX', 'wire_suffix');